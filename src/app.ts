import Koa from 'koa';
import Router from 'koa-router';
import logger from 'koa-logger';
import json from 'koa-json';
import bodyParser from 'koa-bodyparser';

import jwt from 'jsonwebtoken';
import mongodb from 'mongodb';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import { config } from './config';

export const startApp = async (): Promise<void> => {
  // User Interface
  interface User {
    username: string;
    password: string;
  }

  // Koa and Router objects
  const app = new Koa();
  const router = new Router();

  // POST /login
  router.post('/login', async (ctx, next) => {
    const user = ctx.request.body as User;
    await mongodb.MongoClient.connect(config.get('db.url'))
      .then(async client => {
        const db = client.db(config.get('db.name'));
        await db
          .collection(config.get('db.collection'))
          .findOne({ username: user.username })
          .then(async dbuser => {
            await bcrypt
              .compare(user.password, dbuser.password)
              .then(result => {
                if (result === true) {
                  const token = jwt.sign(user, config.get('jwt.secret'), { expiresIn: '1h' });
                  ctx.response.body = { msg: 'You have succesfully logged in. Here is your token:', token };
                } else {
                  ctx.status = 401;
                  ctx.body = {
                    error: 'wrong password',
                  };
                }
              })
              .catch(err => {
                throw err;
              });
          })
          .catch(() => {
            ctx.status = 401;
            ctx.body = {
              error: 'wrong username',
            };
          })
          .finally(() => {
            client.close().catch(err => {
              throw err;
            });
          });
      })
      .catch(err => {
        throw err;
      });
    await next();
  });

  // GET /breweries
  router.get('/breweries', async (ctx, next) => {
    const token = ctx.request.headers['token'];
    try {
      const user = jwt.verify(token, config.get('jwt.secret')) as User;
      await mongodb.MongoClient.connect(config.get('db.url'))
        .then(async client => {
          const db = client.db(config.get('db.name'));
          await db
            .collection(config.get('db.collection'))
            .findOne({ username: user.username })
            .then(async () => {
              if (Object.keys(ctx.request.query).length === 0) {
                await axios
                  .get(`https://api.openbrewerydb.org/breweries/`, { params: ctx.request.query })
                  .then(res => {
                    ctx.status = res.status;
                    ctx.body = res.data;
                  })
                  .catch(err => {
                    throw err;
                  });
              } else {
                await axios
                  .get(`https://api.openbrewerydb.org/breweries/search`, { params: ctx.request.query })
                  .then(res => {
                    ctx.status = res.status;
                    ctx.body = res.data;
                  })
                  .catch(err => {
                    throw err;
                  });
              }
            })
            .catch(err => {
              ctx.status = 401;
              ctx.body = {
                error: 'you are not in the database',
              };
              console.log(err);
            })
            .finally(() => {
              client.close().catch(err => {
                throw err;
              });
            });
        })
        .catch(err => {
          throw err;
        });
    } catch {
      ctx.status = 401;
      ctx.body = {
        error: 'you are not authorized',
      };
    }
    await next();
  });

  // Middlewares
  app.use(json());
  app.use(logger());
  app.use(bodyParser());

  // Router
  app.use(router.routes()).use(router.allowedMethods());

  // Start koa on port 3000
  app.listen(3000, () => {
    console.log('Koa started on port 3000');
  });
};
