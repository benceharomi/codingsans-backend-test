import dotenv from 'dotenv';
import convict from 'convict';

dotenv.config();
export const config = convict({
  jwt: {
    secret: {
      format: String,
      default: '',
      env: 'JWT_SECRET',
    },
  },
  db: {
    name: {
      format: String,
      default: '',
      env: 'DB_NAME',
    },
    collection: {
      format: String,
      default: '',
      env: 'DB_COLLECTION',
    },
    url: {
      format: String,
      default: '',
      env: 'DB_URL',
    },
  },
});

config.validate({ allowed: 'strict' });
export default config.getProperties();
